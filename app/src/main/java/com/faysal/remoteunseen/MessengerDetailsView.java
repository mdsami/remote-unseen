package com.faysal.remoteunseen;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faysal.remoteunseen.adapter.MessageViewAdapter;
import com.faysal.remoteunseen.room_database.Notifications;
import com.faysal.remoteunseen.room_database.NotificationsDatabase;

import java.util.List;


public class MessengerDetailsView extends AppCompatActivity {

    Toolbar mToolbar;

    NotificationsViewModel notificationsViewModel;
    NotificationsDatabase database;

    String userId = null;
    String toolbarTitle = null;
    String packageName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_view);

        mToolbar = findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        Intent intent = getIntent();


        if (intent != null) {

            Bundle bundle = intent.getBundleExtra("data");

            userId = bundle.getString("user_id");
            toolbarTitle = bundle.getString("user_name");
            packageName = bundle.getString("package_name");


            if (packageName.equals("com.facebook.orca")){
                colorOperations(ContextCompat.getColor(this,R.color.messenger),ContextCompat.getColor(this,R.color.messenger));
            }else if (packageName.equals("com.imo.android.imoim")){
                colorOperations(ContextCompat.getColor(this,R.color.imo),ContextCompat.getColor(this,R.color.imo));
            }else if (packageName.equals("com.whatsapp")){
                colorOperations(ContextCompat.getColor(this,R.color.whatsapp),ContextCompat.getColor(this,R.color.whatsapp));
            }else if (packageName.equals("com.instagram.android")){
                colorOperations(ContextCompat.getColor(this,R.color.instagram),ContextCompat.getColor(this,R.color.instagram));
            }

        }


        if (toolbarTitle != null && !TextUtils.isEmpty(toolbarTitle)) {
            getSupportActionBar().setTitle(toolbarTitle);
        }


        database = NotificationsDatabase.getInstance(this);

        RecyclerView recyclerView = findViewById(R.id.messageDetails);
        MessageViewAdapter adapter = new MessageViewAdapter(this);
        notificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel.class);
        notificationsViewModel.getAllNotificationForSpecificUser(userId, packageName).observe(this, list -> adapter.setData(list));
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);


    }

    private void colorOperations(int colorPrimary,int colorAccent){
        mToolbar.setBackgroundColor(colorPrimary);

        Window window = this.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(colorPrimary);
    }

    @Override
    protected void onResume() {
        super.onResume();
        seenOperations();
    }

    public void seenOperations(){
       List<Notifications> list=database.notificationsDao().getAllNotificationForSpecificUserUnseenMessage(userId,packageName,"false");

       for (Notifications notifications : list){
           notifications.setSeenStatus("true");
           database.notificationsDao().updateConversetion(notifications);
       }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.message_details_view, menu);//Menu Resource, Menu

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        switch (item.getItemId()) {
            case R.id.action_delete:
                database.notificationsDao().deleteByUserId(userId);
                startActivity(new Intent(this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
