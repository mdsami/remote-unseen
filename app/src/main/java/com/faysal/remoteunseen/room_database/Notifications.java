package com.faysal.remoteunseen.room_database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "notification")
public class Notifications {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "package")
    public String NotiPackage;

    @ColumnInfo(name = "ticker")
    public String NotiTicker;

    @ColumnInfo(name = "title")
    public String NotiTitle;

    @ColumnInfo(name = "message")
    public String NotiMessage;

    @ColumnInfo(name = "timestamp")
    public String NotiTimestamp;

    @ColumnInfo(name = "notifyid")
    public String notificationsId;

    @ColumnInfo(name = "seen_status")
    public String seenStatus;



    public Notifications() {
    }

    public Notifications(String notiPackage, String notiTicker, String notiTitle, String notiMessage, String notiTimestamp, String notificationsId,String seenStatus) {
        this.id = id;
        NotiPackage = notiPackage;
        NotiTicker = notiTicker;
        NotiTitle = notiTitle;
        NotiMessage = notiMessage;
        NotiTimestamp = notiTimestamp;
        this.notificationsId = notificationsId;
        this.seenStatus=seenStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNotiPackage() {
        return NotiPackage;
    }

    public void setNotiPackage(String notiPackage) {
        NotiPackage = notiPackage;
    }

    public String getNotiTicker() {
        return NotiTicker;
    }

    public void setNotiTicker(String notiTicker) {
        NotiTicker = notiTicker;
    }

    public String getNotiTitle() {
        return NotiTitle;
    }

    public void setNotiTitle(String notiTitle) {
        NotiTitle = notiTitle;
    }

    public String getNotiMessage() {
        return NotiMessage;
    }

    public void setNotiMessage(String notiMessage) {
        NotiMessage = notiMessage;
    }

    public String getNotiTimestamp() {
        return NotiTimestamp;
    }

    public void setNotiTimestamp(String notiTimestamp) {
        NotiTimestamp = notiTimestamp;
    }

    public String getNotificationsId() {
        return notificationsId;
    }

    public void setNotificationsId(String notificationsId) {
        this.notificationsId = notificationsId;
    }

    public String getSeenStatus() {
        return seenStatus;
    }

    public void setSeenStatus(String seenStatus) {
        this.seenStatus = seenStatus;
    }

    @Override
    public String toString() {
        return "Notifications{" +
                "id=" + id +
                ", NotiPackage='" + NotiPackage + '\'' +
                ", NotiTicker='" + NotiTicker + '\'' +
                ", NotiTitle='" + NotiTitle + '\'' +
                ", NotiMessage='" + NotiMessage + '\'' +
                ", NotiTimestamp='" + NotiTimestamp + '\'' +
                ", notificationsId='" + notificationsId + '\'' +
                '}';
    }
}
