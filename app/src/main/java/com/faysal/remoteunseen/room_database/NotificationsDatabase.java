package com.faysal.remoteunseen.room_database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

@Database(entities = Notifications.class,exportSchema = false,version = 2)
public abstract class NotificationsDatabase extends RoomDatabase {

    public static final String DATABASE_NAME="notification.db";
    private static NotificationsDatabase instance;


    public static synchronized NotificationsDatabase getInstance(Context context){
        if (instance==null){
           instance = Room.databaseBuilder(context.getApplicationContext(), NotificationsDatabase.class, DATABASE_NAME)
                    // allow queries on the main thread.
                    // Don't do this on a real app! See PersistenceBasicSample for an example.
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }

    public abstract NotificationsDao notificationsDao();

    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }
}
