package com.faysal.remoteunseen.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.faysal.remoteunseen.MessengerDetailsView;
import com.faysal.remoteunseen.R;
import com.faysal.remoteunseen.room_database.Notifications;
import com.faysal.remoteunseen.room_database.NotificationsDatabase;
import com.faysal.remoteunseen.utils.DataOperations;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MessageViewAdapter extends RecyclerView.Adapter<MessageViewAdapter.MessageViewHolder>{

    Context con;
    List<Notifications> listData;

    public static final String TAG="Afsaa";


    boolean yesterdayDateAtOnce=true;
    boolean todayDateAtOnce=true;

    public MessageViewAdapter(Context con) {
        this.con = con;
        listData = new ArrayList<>();
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MessageViewHolder(LayoutInflater.from(con).inflate(R.layout.message_view_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {

        if (position==0){
            holder.chat_shape_layout.setBackgroundResource(R.drawable.their_message);
        }else if ((getItemCount()-1)==position){
            holder.chat_shape_layout.setBackgroundResource(R.drawable.last_message_shape);
        }else {
            holder.chat_shape_layout.setBackgroundResource(R.drawable.rounded_corner);
        }


        Notifications notifications=listData.get(position);
        holder.bind(notifications);
        Log.d("Shakil", "onBindViewHolder: "+notifications.NotiMessage);

    }



    public void setData(List<Notifications> newData) {
        this.listData = newData;
        ((MessengerDetailsView)con).seenOperations();
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    class MessageViewHolder extends RecyclerView.ViewHolder{

        TextView message;
        TextView date;
        View view;
        TextView messageHistoryDate;
        LinearLayout chat_shape_layout;


        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);
            view=itemView;
            message=itemView.findViewById(R.id.message);
            date=itemView.findViewById(R.id.date);
            chat_shape_layout=itemView.findViewById(R.id.chat_shape_layout);
            messageHistoryDate=itemView.findViewById(R.id.messageHistoryDate);


        }
        void bind(final Notifications notifications) {
            NotificationsDatabase database=NotificationsDatabase.getInstance(con);

            if (notifications != null) {
                message.setText(notifications.getNotiMessage());


                String dateOfNoti=notifications.getNotiTimestamp().split("\\s+")[0];
                String time=notifications.getNotiTimestamp().split("\\s+")[1];
                time=time.split(":+")[0]+":"+time.split(":+")[1];
                String amPm=notifications.getNotiTimestamp().split("\\s+")[2];




                if (getTodayDate().equals(dateOfNoti)){
                    date.setText("Today "+time+" "+amPm);
                }else if (getYesterdayDateString().equals(dateOfNoti)){
                    date.setText("Yesterday "+time+" "+amPm);
                }else {
                    date.setText(DataOperations.dateWithMonth(dateOfNoti)+" , "+time+" "+amPm);
                }

                Log.d(TAG, "Seen Status : "+notifications.getSeenStatus());


               // Log.d(TAG, "bind: "+getYesterdayDateString()+"---------"+dateOfNoti);



            }



        }
        private String getTodayDate(){
            Date today = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
            String dateToStr = format.format(today);

            return dateToStr.split("\\s+")[0];
        }


        private String getYesterdayDateString() {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            return dateFormat.format(yesterday()).split("\\s+")[0].replace("/","-");
        }

        private Date yesterday() {
            final Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            return cal.getTime();
        }

    }
}
