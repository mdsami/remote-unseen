package com.faysal.remoteunseen.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.faysal.remoteunseen.DetailsView;
import com.faysal.remoteunseen.MessengerDetailsView;
import com.faysal.remoteunseen.NotificationsViewModel;
import com.faysal.remoteunseen.R;
import com.faysal.remoteunseen.room_database.Notifications;
import com.faysal.remoteunseen.room_database.NotificationsDatabase;
import com.faysal.remoteunseen.utils.DataOperations;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MessengerContactViewAdapter extends RecyclerView.Adapter<MessengerContactViewAdapter.MessengerContactViewHolder>{

    Context con;
    List<Notifications> listData;


    NotificationsViewModel notificationsViewModel;

    public MessengerContactViewAdapter(Context con) {
        this.con = con;
        listData = new ArrayList<>();
    }

    @NonNull
    @Override
    public MessengerContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MessengerContactViewHolder(LayoutInflater.from(con).inflate(R.layout.item_unseen_contact_paricular_app,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MessengerContactViewHolder holder, int position) {

        Notifications notifications=listData.get(position);
        holder.bind(notifications);
        Log.d("Shakil", "onBindViewHolder: "+notifications.getNotificationsId());


        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(con, MessengerDetailsView.class);
                /*intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("user_id",notifications.getNotificationsId());
                intent.putExtra("user_name",notifications.getNotiTitle());
                intent.putExtra("package_name",notifications.getNotiPackage());*/


                Bundle bundle=new Bundle();
                bundle.putString("user_id",notifications.getNotificationsId());
                bundle.putString("user_name",notifications.getNotiTitle());
                bundle.putString("package_name",notifications.getNotiPackage());
                intent.putExtra("data",bundle);
                con.startActivity(intent);


            }
        });

    }

    public void setData(List<Notifications> newData) {
        Collections.reverse(newData);
        this.listData = removeDuplicate(removeMessangerOwnNOtification(newData));
        notifyDataSetChanged();
    }

    private List<Notifications> removeMessangerOwnNOtification(List<Notifications> notificationsList){
       /* for ( Notifications n : notificationsList){
            if (n.getNotiTitle().equals("Chat heads active") || n.getNotiTitle().equals("Messenger")){
                notificationsList.remove(n);
            }
        }*/

      /* for (int i=0; i<notificationsList.size(); i++){
           if (notificationsList.get(i).getNotificationsId().equals("chatheadsactive") || notificationsList.get(i).getNotificationsId().equals("messenger")){
               notificationsList.remove(i);
           }
       }*/
        return notificationsList;
    }

    private List<Notifications> removeDuplicate(List<Notifications> allNotifications){
        List<Notifications> noRepeat = new ArrayList<Notifications>();

        for (Notifications event : allNotifications) {
            boolean isFound = false;
            // check if the event name exists in noRepeat
            for (Notifications e : noRepeat) {
                if (e.getNotificationsId().equals(event.getNotificationsId()) && e.getNotiPackage().equals(event.getNotiPackage()) || (e.equals(event))) {
                    isFound = true;
                    break;
                }
            }
            if (!isFound) noRepeat.add(event);
        }
        return noRepeat;
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }


    class MessengerContactViewHolder extends RecyclerView.ViewHolder{

        ImageView appIcon;
        TextView contactName;
        TextView timeStamp;
        TextView contactLastMessage;
        TextView messageCount;

        View view;

        LinearLayout root;

        public MessengerContactViewHolder(@NonNull View itemView) {
            super(itemView);
            view=itemView;
            appIcon=itemView.findViewById(R.id.app_icon);
            contactName=itemView.findViewById(R.id.contact_name);
            timeStamp=itemView.findViewById(R.id.time_stamp);
            contactLastMessage=itemView.findViewById(R.id.contact_last_message);
            messageCount=itemView.findViewById(R.id.message_count);
            root=itemView.findViewById(R.id.root);


        }
        void bind(final Notifications notifications) {
            NotificationsDatabase database=NotificationsDatabase.getInstance(con);


            if (notifications != null) {
                contactName.setText(notifications.getNotiTitle());
                //timeStamp.setText(notifications.NotiTimestamp);




                List<Notifications> notiList=database.notificationsDao().getAllNotificationForSpecificUserFS(notifications.getNotificationsId(),notifications.getNotiPackage());

                int counts=database.notificationsDao().getAllNotificationForSpecificUserUnseenMessage(notifications.getNotificationsId(),notifications.getNotiPackage(),"false").size();
                String lastMessage=notiList.get(notiList.size()-1).getNotiMessage();
                String lastMessageSeenStatus=notiList.get(notiList.size()-1).getSeenStatus();

                if ((lastMessage != null && !lastMessage.isEmpty() && !lastMessage.equals("null"))){
                    if (lastMessageSeenStatus.equals("false")){
                        contactLastMessage.setTextColor(ContextCompat.getColor(con,R.color.colorBlack));
                        contactLastMessage.setTypeface(contactLastMessage.getTypeface(), Typeface.BOLD);
                        timeStamp.setTextColor(ContextCompat.getColor(con,R.color.messenger));
                    }else {
                        contactLastMessage.setTextColor(ContextCompat.getColor(con,R.color.last_message_color));
                        contactLastMessage.setTypeface(null, Typeface.NORMAL);
                        timeStamp.setTextColor(ContextCompat.getColor(con,R.color.timestamp_text_color));
                    }
                    contactLastMessage.setText(lastMessage);
                }

                messageCount.setText(counts+"");

                if (counts!=0){
                    if (counts>99){
                        messageCount.setText("99+");
                    }else {
                        messageCount.setText(counts+"");
                    }
                    messageCount.setBackgroundResource(R.drawable.messenger_circle_tv);
                }else {
                    messageCount.setBackgroundResource(R.drawable.white_circle_tv);
                }


                try {
                    TextDrawable drawable = (TextDrawable) TextDrawable.builder()
                            .buildRoundRect(String.valueOf(notifications.getNotiTitle().charAt(0)), ContextCompat.getColor(con,R.color.messenger),100);
                    appIcon.setImageDrawable(drawable);
                }catch (Exception e){

                    TextDrawable drawable = (TextDrawable) TextDrawable.builder()
                            .buildRoundRect("NU", ContextCompat.getColor(con,R.color.colorPrimary),100);
                    appIcon.setImageDrawable(drawable);
                }


                String dateOfNoti=notifications.getNotiTimestamp().split("\\s+")[0];
                String time=notifications.getNotiTimestamp().split("\\s+")[1];
                time=time.split(":+")[0]+":"+time.split(":+")[1];
                String amPm=notifications.getNotiTimestamp().split("\\s+")[2];



                if (getTodayDate().equals(dateOfNoti)){
                    timeStamp.setText(time+" "+amPm);
                }else if (getYesterdayDateString().equals(dateOfNoti)){
                    timeStamp.setText("Yesterday ");
                }else {
                    timeStamp.setText(DataOperations.dateWithMonth(dateOfNoti));
                }

            }
        }

    }


    private String getTodayDate(){
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        String dateToStr = format.format(today);

        return dateToStr.split("\\s+")[0];
    }


    private String getYesterdayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return dateFormat.format(yesterday()).split("\\s+")[0].replace("/","-");
    }

    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }
}
