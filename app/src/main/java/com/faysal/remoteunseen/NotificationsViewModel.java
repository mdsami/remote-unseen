package com.faysal.remoteunseen;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.faysal.remoteunseen.room_database.Notifications;
import com.faysal.remoteunseen.room_database.NotificationsDao;
import com.faysal.remoteunseen.room_database.NotificationsDatabase;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NotificationsViewModel extends AndroidViewModel {

    private NotificationsDao notificationsDao;
    private ExecutorService executorService;

    public NotificationsViewModel(@NonNull Application application) {
        super(application);
        notificationsDao = NotificationsDatabase.getInstance(application).notificationsDao();
        executorService = Executors.newSingleThreadExecutor();
    }

    public LiveData<List<Notifications>> getAllPosts() {
        return notificationsDao.getAll();
    }

    public LiveData<List<Notifications>> getAllNotificationByPackage(String packageName) {
        return notificationsDao.getAllNotificationByPackage(packageName);
    }


    LiveData<List<Notifications>> getAllNotificationForSpecificUser(String notiId,String packageName) {
        return notificationsDao.getAllNotificationForSpecificUser(notiId,packageName);
    }


    void savePost(Notifications notification) {
        executorService.execute(() -> notificationsDao.insertNotifications(notification));
    }

    void deletePost(Notifications notification) {
        executorService.execute(() -> notificationsDao.deleteNotifications(notification));
    }


}
