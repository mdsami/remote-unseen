package com.faysal.remoteunseen;

import android.app.Notification;
import android.content.Context;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.faysal.remoteunseen.room_database.Notifications;
import com.faysal.remoteunseen.room_database.NotificationsDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;


public class NotificationService extends NotificationListenerService {

    Context context;
    String title;
    String text;
    String packageName;
    String ticker;

    public static final String TAG=NotificationService.class.getSimpleName();

    @Override

    public void onCreate() {

        super.onCreate();
        context = getApplicationContext();

    }
/*
    @Override

    public void onNotificationPosted(StatusBarNotification sbn) {
        String pack = sbn.getPackageName();
        String ticker ="";
        if(sbn.getNotification().tickerText !=null) {
            ticker = sbn.getNotification().tickerText.toString();
        }


        Bundle extras = sbn.getNotification().extras;

        if (extras !=null){

        }


        if ((title !=null || !title.equals("")) && (text !=null || !text.equals(""))){
            if (pack.equals("com.facebook.orca")){

                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                String dateToStr = format.format(today);

                String notifyId=title.replaceAll("\\s+","").toLowerCase();

                NotificationsDatabase database=NotificationsDatabase.getInstance(getApplicationContext());
                Notifications notifications=new Notifications(pack,ticker,title,text,dateToStr,notifyId);
                database.notificationsDao().insertNotifications(notifications);

                Log.d("Afsana", "Post notification Time ");
            }
        }







        */
/*int id1 = extras.getInt(Notification.EXTRA_SMALL_ICON);
        Bitmap id = sbn.getNotification().largeIcon;*//*








        Log.i("Package",pack);
        Log.i("Ticker",ticker);
        Log.i("Title",title);
        Log.i("Text",text);

        */
/*Intent msgrcv = new Intent("Msg");
        msgrcv.putExtra("package", pack);
        msgrcv.putExtra("ticker", ticker);
        msgrcv.putExtra("title", title);
        msgrcv.putExtra("text", text);
        if(id != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            id.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            msgrcv.putExtra("icon",byteArray);
        }
        LocalBroadcastManager.getInstance(context).sendBroadcast(msgrcv);*//*



    }
*/



    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {

       /* Log.i(TAG,"**********  onNotificationPosted");
        Log.i(TAG,"ID :" + sbn.getId() + "\t" + sbn.getNotification().tickerText + "\t" + sbn.getPackageName());
        Intent i = new  Intent("com.github.chagall.notificationlistenerexample.NOTIFICATION_LISTENER_EXAMPLE");
        i.putExtra("notification_event","onNotificationPosted :" + sbn.getPackageName() + "\n");
        sendBroadcast(i);*/

        if (sbn !=null){

            try {
                title=sbn.getNotification().extras.getString(Notification.EXTRA_TITLE).toString();
            }catch (Exception e){
                title=null;
            }

            try {
                text=sbn.getNotification().extras.getString(Notification.EXTRA_TEXT).toString();
            }catch (Exception e){
                text=null;
            }

            if(sbn.getNotification().tickerText !=null) {
                ticker = sbn.getNotification().tickerText.toString();
            }



            packageName=sbn.getPackageName();



            //Log.d(TAG, "Outside Title : "+title+" Message "+text+" Package Name "+packageName);


            if ((title != null && !title.isEmpty() && !title.equals("null")) && (text != null && !text.isEmpty() && !text.equals("null")) ){

                if (packageName.equals("com.facebook.orca") || packageName.equals("com.imo.android.imoim") || packageName.equals("com.whatsapp") || packageName.equals("com.instagram.android")){

                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    String dateToStr = format.format(today);

                    String notifyId=title.replaceAll("\\s+","").toLowerCase();

                    if (!notifyId.equals("chatheadsactive") || notifyId.equals("messenger")){
                        NotificationsDatabase database=NotificationsDatabase.getInstance(getApplicationContext());
                        Notifications notifications=new Notifications(packageName,ticker,title,text,dateToStr,notifyId,"false");
                        database.notificationsDao().insertNotifications(notifications);
                    }



                    Log.d(TAG, "Inside Title : "+title+" Message "+text+" Package Name "+packageName);

                }


            }








        }

    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
      /*  Log.i(TAG,"********** onNOtificationRemoved");
        Log.i(TAG,"ID :" + sbn.getId() + "\t" + sbn.getNotification().tickerText +"\t" + sbn.getPackageName());
        Intent i = new  Intent("com.github.chagall.notificationlistenerexample.NOTIFICATION_LISTENER_EXAMPLE");
        i.putExtra("notification_event","onNotificationRemoved :" + sbn.getPackageName() + "\n");

        sendBroadcast(i);*/
    }

}
