package com.faysal.remoteunseen;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.faysal.remoteunseen.room_database.Notifications;

import java.util.List;

public class ContactViewAdapter extends RecyclerView.Adapter<ContactViewAdapter.ContactViewHolder> {

    Context context;
    List<Notifications> listData;


    public ContactViewAdapter(Context context, List<Notifications> listData) {
        this.context = context;
        this.listData = listData;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ContactViewHolder(LayoutInflater.from(context).inflate(R.layout.item_unseen_contact,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        Notifications notifications=listData.get(position);
        holder.contactName.setText(notifications.getNotiTitle());
        holder.contactLastMessage.setText(notifications.getNotiMessage());

        TextDrawable drawable = TextDrawable.builder()
                .buildRect(String.valueOf(notifications.getNotiTitle().charAt(0)), Color.RED);
        holder.appIcon.setImageDrawable(drawable);

        Log.d("Shakil", "onBindViewHolder: "+notifications.NotiMessage);
    }

    @Override
    public int getItemCount() {
        return 0;
    }


    class ContactViewHolder extends RecyclerView.ViewHolder{

        ImageView appIcon;
        TextView contactName;
        TextView timeStamp;
        TextView contactLastMessage;
        TextView messageCount;

        View view;


        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);

            view=itemView;
            appIcon=itemView.findViewById(R.id.app_icon);
            contactName=itemView.findViewById(R.id.contact_name);
            timeStamp=itemView.findViewById(R.id.time_stamp);
            contactLastMessage=itemView.findViewById(R.id.contact_last_message);
            messageCount=itemView.findViewById(R.id.message_count);

        }
    }
}
